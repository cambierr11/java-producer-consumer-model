import java.util.Random;
class Producer extends Thread{
    BoundedBuffer b = null;
    public Producer(BoundedBuffer initb) {
        b = initb;
        new Thread(this).start();
	
    }
    public void run() {
	String threadone = new Thread(this).getName();
        double item;
        Random r = new Random();
        while (true) {
            item = r.nextDouble();
	if (Symbols.debugFlag)
	{
	System.out.print(threadone + " ");
            System.out.println("produced item " + item);
	}
            b.deposit(item);
            Util.mySleep(200);
        }
    }
}

class Consumer extends Thread {

    BoundedBuffer b = null;
    public Consumer(BoundedBuffer initb) {
        b = initb;
        new Thread(this).start();
    }
    public void run() {
	String threadone = new Thread(this).getName();
        double item;
        while (true) {
            item = b.fetch();
	if (Symbols.debugFlag)
	{
	System.out.print(threadone + " ");
            System.out.println("fetched item " + item);
	}
             Util.mySleep(50);
        }
    }
}
class ProducerConsumer {
    public static void main(String[] args) {

	Symbols.debugFlag = Boolean.parseBoolean(args[0]);
	
	Symbols.consumerNum = Integer.parseInt(args[1]);

        BoundedBuffer buffer = new BoundedBuffer();
        Producer producer = new Producer(buffer);
        Consumer consumer = new Consumer(buffer);
	Consumer consumer2 = new Consumer(buffer);
    }
}

